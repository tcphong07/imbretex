(function ($) {

    var form_inscription = document.getElementById("form-prospect");
    if (form_inscription == null) {
        var form_id = 'form-prospect-modal';
        is_modal = true;
        var modal = '-modal';
        var modal_inderscore = '_modal';
    } else {
        var form_id = 'form-prospect';
        is_modal = false;
        var modal = '';
        var modal_inderscore = '';
        $('#content-modal-prospect').empty().append('<div class="block-desc">' + document.getElementById('empty-modal').value + '</div>');
    }
    updateCartPriceStick();
    setRequiredField(is_modal);
    if (is_modal) {
        document.getElementById('reg_file_modal').style.display = 'none';
    } else {
        document.getElementById('reg_file').style.display = 'none';
    }


    $('.file-link' + modal + '').on('click', function (e) {
        $('#reg_file' + modal_inderscore + '').trigger('click');
        // document.getElementById('reg_file').style.display = 'none';
        $('#reg_file' + modal_inderscore + '').change(function () {
            var filename = $('#reg_file' + modal_inderscore + '').val();
            if (filename.substring(3, 11) == 'fakepath') {
                filename = filename.substring(12);
            } // Remove c:\fake at beginning from localhost chrome
            $('#file-name' + modal + '').empty().html("      " + filename);
        });

    });

    $('.text-capitalize').keyup(function () {
        $(this).val($(this).val().toUpperCase());
    });

    $(".no-whitespace").keyup(function () {
        $(this).val($(this).val().replace(" ", ""));
    });

    $('.txt-control').keyup(function () {
        var value = jQuery(this).val();
        value = value.replace(',', '');
        jQuery(this).val(value);
    });

    function init() {
        $(".chiffres").keyup(function (event) {
            var value = jQuery(this).val();
            value = value.replace(/[^0-9]+/g, '');
            jQuery(this).val(value);
        });
    }

    init();
    $('#' + form_id + '').on('submit', function (e) {
        e.preventDefault();
        var modal = '';
        if (is_modal) {
            var $eecField = $("#reg_eec_num_modal"),
                $siretField = $("#reg_siret_modal");
            modal = '-modal';
        } else {
            var $eecField = $("#reg_eec_num"),
                $siretField = $("#reg_siret");
        }

        var canSubmit = true,
            captchaLength = $(".g-recaptcha").length;
        if (captchaLength > 0) {
            if (grecaptcha.getResponse() === "") {
                toastr.error("La validation du reCaptcha est obligatoire");
                canSubmit = false;
            }
        } else {
            $('.prospect-button').prop('disabled', false);
            $('.prospect-button').empty().append('Envoyer la demande');
        }

        // on enlève les espaces si un copier-collé a été fait
        $eecField.val($eecField.val().replace(" ", ""));
        $siretField.val($siretField.val().replace(" ", ""));
        var country_id = "country-field" + modal;
        if ($("#" + country_id + "").val() !== "19") {
            // ce n'est pas la france qui est sélectionné, on doit avoir le champs eec rempli
            if ($eecField.val() === "") {
                toastr.error("Le champs \"TVA intracommunautaire\" est obligatoire.");
                canSubmit = false;
            }
        } else {
            // la france est selectionné, on doit avoir le champs siret rempli
            if ($siretField.val() === "") {
                toastr.error("Le champs \"Siret\" est obligatoire.");
                canSubmit = false;
            }
        }

        if (canSubmit) {
            $('.prospect-button').prop('disabled', true);
            var treatment = document.getElementById('on-going-treatment').value;
            $('.prospect-button').empty().append('<i class="fa fa-spinner fa-spin"></i>' + treatment + '');
            document.getElementById("prospect-button").style.pointerEvents = "none";
            $.ajax({
                url: '/ajouter-prospect',                           // Any URL
                method: 'POST',                           // Any URL
                // data:  $(this).serialize(),                          // Serialize the form data
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (data) {
                    if (data[1] !== undefined) {
                        $('#error-account' + modal + '').empty().append(data[1]);
                        $('.prospect-button').prop('disabled', false);
                        $('.prospect-button').empty().append('Envoyer la demande');
                        if (data[2] === 0) {
                            $('#prospect-button-validate').empty();
                        }
                        if (data[2] != 0) {
                            grecaptcha.reset();
                            document.getElementById("prospect-button").style.pointerEvents = "auto";
                        }
                        if (is_modal) {
                            $('#create_account').empty();
                            $('#modalLogin').on('hidden.bs.modal', function (e) {
                                location.reload();
                            });
                        }
                    } else {
                        if (is_modal) {
                            $('#modal-prospect').empty().append(data[0]);
                            $('#modalRegistration').find('.modal-dialog.modal-dialog-centered').addClass('confirmed');
                            $('#modalRegistration').on('hidden.bs.modal', function (e) {
                                location.reload();
                            });
                        } else {
                            window.location.href = $('#inscription').data('href');
                        }
                    }
                },
                error: function (xhr, text, error) {              // If 40x or 50x; errors
                    alert('Error: ' + error);
                }
            });
        }

    });

    /* Afficher la modale de connexion si on une erreur d'authentification */
    var session_error = $('#check-login');
    var session_value_error = document.getElementById('check-login').value;
    var login = session_error.data('login');
    if (session_value_error != "" && !login) {
        $('#modalLogin').modal('show');
    }

    $('#country-field' + modal + '').on('change', function (e) {
        setRequiredField(is_modal);
    });

    function setRequiredField(is_modal) {
        var texte;
        var value;
        if (is_modal) {
            texte = document.getElementById("country-field-modal").options[document.getElementById('country-field-modal').selectedIndex];
        } else {
            texte = document.getElementById("country-field").options[document.getElementById('country-field').selectedIndex];
        }

        if (texte) {
            value = texte.text;
        } else {
            value = '';
        }
        $.ajax({
            url: '/required-fields',
            data: {
                country_id: texte.value,
            },
            success: function (data) {
                // siren obligatoire ou pas
                if (data['siren_required'] === true) {
                    if (is_modal) {
                        document.getElementById("reg_siret_modal").attributes["required"] = "required";
                        document.getElementById("reg_file_modal").attributes["required"] = "required";
                        document.getElementById('aster_siret_modal').style.display = '';
                    } else {
                        document.getElementById("reg_siret").attributes["required"] = "required";
                        document.getElementById("reg_file").attributes["required"] = "required";
                        document.getElementById('aster_siret').style.display = '';
                    }
                } else {
                    if (is_modal) {
                        document.getElementById("reg_siret_modal").removeAttribute('required');
                        document.getElementById("reg_file_modal").removeAttribute('required');
                        document.getElementById('aster_siret_modal').style.display = 'none';
                    } else {
                        document.getElementById("reg_siret").removeAttribute('required');
                        document.getElementById("reg_file").removeAttribute('required');
                        document.getElementById('aster_siret').style.display = 'none';
                    }
                }
                if (is_modal) {
                    document.getElementById("reg_siret_modal").removeAttribute('maxLength');
                    document.getElementById("reg_siret_modal").removeAttribute('minLength');
                } else {
                    document.getElementById("reg_siret").removeAttribute('maxLength');
                    document.getElementById("reg_siret").removeAttribute('minLength');
                }

                // nombre de catactères max de siren
                if (is_modal) {
                    document.getElementById("reg_siret_modal").removeAttribute('pattern');
                    $("#reg_siret_modal").off("keyup");
                    if (data['siren_is_numeric'] == 1) {
                        $("#reg_siret_modal").keyup(function (event) {
                            var value = jQuery(this).val();
                            value = value.replace(/[^0-9]+/g, '');
                            jQuery(this).val(value);
                        });

                    }
                } else {
                    document.getElementById("reg_siret").removeAttribute('pattern');
                    $("#reg_siret").off('keyup');
                    if (data['siren_is_numeric'] == 1) {
                        $("#reg_siret").keyup(function (event) {
                            var value = jQuery(this).val();
                            value = value.replace(/[^0-9]+/g, '');
                            jQuery(this).val(value);
                        });
                    }
                }
                if (data['number_char_siren'] == -1  ) {
                    $('#siren-comment').empty();
                } else {
                    if(data['display_siren_comment'] == 1)
                    {
                        $("#siren-comment").empty().append('(' + data['number_char_siren'] + document.getElementById("siret_number").value + ')');
                    }else{
                        $('#siren-comment').empty();
                    }
                    if (is_modal) {
                        document.getElementById("reg_siret_modal").maxLength = data['number_char_siren'];
                        document.getElementById("reg_siret_modal").minLength = data['number_char_siren'];
                        document.getElementById("reg_siret_modal").removeAttribute('pattern');
                        if(data['number_char_siren'] == 9) {
                            document.getElementById("reg_siret_modal").pattern = '\\d{' + data['number_char_siren'] + '}';
                        }

                    } else {
                        document.getElementById("reg_siret").maxLength = data['number_char_siren'];
                        document.getElementById("reg_siret").minLength = data['number_char_siren'];
                        document.getElementById("reg_siret").removeAttribute('pattern');
                        if(data['number_char_siren'] == 9) {
                            document.getElementById("reg_siret").pattern = "\\d{" + data['number_char_siren'] + "}";
                        }
                    }
                }
                // siren obligatoire ou pas
                if (data['required_eec_num'] === true) {
                    if (is_modal) {
                        document.getElementById("reg_eec_num_modal").attributes["required"] = "required";
                        document.getElementById('aster_eec_modal').style.display = '';
                    } else {
                        document.getElementById("reg_eec_num").attributes["required"] = "required";
                        document.getElementById('aster_eec').style.display = '';
                    }
                } else {
                    if (is_modal) {
                        document.getElementById("reg_eec_num_modal").removeAttribute('required');
                        document.getElementById('aster_eec_modal').style.display = 'none';
                    } else {
                        document.getElementById("reg_eec_num").removeAttribute('required');
                        document.getElementById('aster_eec').style.display = 'none';
                    }
                }
                if (is_modal) {
                    document.getElementById("reg_eec_num_modal").removeAttribute('maxLength');
                    document.getElementById("reg_eec_num_modal").removeAttribute('minLength');
                } else {
                    document.getElementById("reg_eec_num").removeAttribute('maxLength');
                    document.getElementById("reg_eec_num").removeAttribute('minLength');
                }
                if (data['number_eec'] == -1) {
                    $('#number-char-eec_num').empty();
                } else {
                    if (is_modal) {
                        document.getElementById("reg_eec_num_modal").maxLength = data['number_eec'];
                        document.getElementById("reg_eec_num_modal").minLength = data['number_eec'];
                    } else {
                        document.getElementById("reg_eec_num").maxLength = data['number_eec'];
                        document.getElementById("reg_eec_num").minLength = data['number_eec'];
                    }
                    $('#number-char-eec_num').empty().append('(' + data['number_eec'] + document.getElementById("siret_number").value + ')');
                }

            }
        });
    }

    function updateCartPriceStick() {

        $.ajax({
            url: '/panier/SyncCart',
            method: 'GET',
            success: function (data) {

            }
        });
    }


})(jQuery);