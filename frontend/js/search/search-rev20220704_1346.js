$(document).ready(function () {
    let categories = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: `${$('form#search-form').attr('action')}?keyword=%QUERY%&search=suggestion&team=categories`,
            wildcard: '%QUERY%'
        }
    });

    let products = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: `${$('form#search-form').attr('action')}?keyword=%QUERY%&search=suggestion&team=products`,
            wildcard: '%QUERY%'
        }
    });

    $('form#search-form input').typeahead({
        hint: true,
        highlight: true,
        minLength: 3
    }, {
        name: 'categories',
        limit: 10,
        display: 'name',
        source: categories,
        templates: {
            header: '<div class="list-group-item list-group-item-heading" style="background-color: #ebeff2"><b>'+document.getElementById("header-categories").value+'</b></div>',
            suggestion: function (data) {
                if (data !== undefined) {
                    if ( data['parent_name'] !== null)
                    {
                        return `<a href="/produits/${slugify(data.name)}_${data.id}"><div class="list-group-item list-group-item-action">${data.parent_name}  >  ${data.name}</div></a>`;

                    }else{

                        return `<a href="/produits/${slugify(data.name)}_${data.id}"><div class="list-group-item list-group-item-action" > ${data.name}</div></a>`;
                    }

                }
            }
        }
    }, {
        name: 'products',
        limit: 100,
        display: 'name',
        source: products,
        templates: {
            header: '<div class="list-group-item list-group-item-heading" style="background-color: #ebeff2"><b>'+document.getElementById("header-products").value+'</b></div>',
            suggestion: function (data) {
                return `<a href="/produits/${data.id}"><div class="list-group-item list-group-item-action">${data.reference} - ${data.name}</div></a>`;
            }
        }
    }).on('typeahead:selected', function() {
        $('form#search-form').submit();
    });

    function slugify($str) {
        return $str.toLowerCase()
            .replace(/[ _]/g, '-')
            .replace( /-+/g, '-')
            .replace(/[^\w-]+|^-+|-+$/g, '');
    }

    $( "#search-form-slide" ).click(function() {
        var elm = document.getElementById("search-form-header");
        elm.focus();
    });

    $("#search-form-header").on('keyup',function(e) {
        document.getElementById("search-form-slide").value = $("#search-form-header").val();
        // elm.text;
    });
}

);
