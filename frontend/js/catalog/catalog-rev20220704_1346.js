var ImbretexFrontCatalog = function () {

    this.initEvents= function () {
        $("#open-catalog-modal").on('click',function (e) {
            e.preventDefault();
            HegydCore.initRemoteModal('#modalContainer', $(this).data('href'));
        });
    };

    return {
        init: function () {
            self.initEvents();
        }
    }
}();

$(document).ready(function () {
    ImbretexFrontCatalog.init();
});