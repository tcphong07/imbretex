window.MoreIdeasBlock = {

    hashKey     : '#&p=1&q=16&t=1',
    queryString : '?',

    generateParameters : function(){

        let self = this;
        let ids = [
            'category',
            'color',
            'trade',
            'gender',
            'size'
        ];

        ids.forEach(function (element) {
            let $select = $("#" + element + "_idea");
            if($select.val()) {
                self.hashKey += "&f" + $select.data('filter') + "=" + $select.val().toLowerCase();
                self.queryString += "&" + $select.data('filter') + "=" + $select.val().toLowerCase();
            }
        });

        return self.queryString + self.hashKey
    },

    initEvents: function(){

        $('#more_ideas_form button[type="submit"]').on('click',function (e) {
            e.preventDefault();

            let self = this;
            let urlParams = MoreIdeasBlock.generateParameters();

            let url = $('#more_ideas_form button[type="submit"]').data('href') + urlParams;
            window.location = url;
        });

        $('.see-more-button').on('click',function (e) {
            e.preventDefault();

            let $seeMoreOptions = $('#see-more-options');
            let $seeMoreButton = $('.see-more-button');

            if($seeMoreOptions.hasClass('d-none')){
                $seeMoreOptions.removeClass('d-none');
                $seeMoreButton.find('.see-more-span').addClass('d-none');
                $seeMoreButton.find('.see-less-span').removeClass('d-none');
            } else{
                $seeMoreOptions.addClass('d-none');
                $seeMoreButton.find('.see-more-span').removeClass('d-none');
                $seeMoreButton.find('.see-less-span').addClass('d-none');
            }
        });
    },

    init: function () {
        this.initEvents();
    }

};


(function ($) {
    MoreIdeasBlock.init();
})(jQuery);