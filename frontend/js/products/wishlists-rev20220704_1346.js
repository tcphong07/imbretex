$(document).ready(function($){

    $('.wishlist').on("click", function(){

        var $this = $(this);
        var id = $(this).data('id');

        if ($(this).hasClass('ico_ico_solidheart')) {
            removeWishlist($this, id);
        }
        else {
            addWishlist($this, id);
        }

        return false;
    });

    $('.main').on("click", '.heart-icon', function(){
        var $this = $(this).find('i');
        var id = $this.data('id');
        var login = $this.data('login');

        if (!login) {
            $('#modalLogin').modal('show');
            return false;
        }

        if ($this.hasClass('ico_ico_solidheart')) {
            removeWishlist($this, id);
        }
        else {
            addWishlist($this, id);
        }

        return false;

    });

});

function addWishlist($this, id)
{
    var url = '/wishlist/'+ id;
    $.ajax({
        url: url,
        method: 'POST',
        data: {},
    }).success(function (data) {
        if (data.success) {
            $this.addClass('ico_ico_solidheart');
            $this.removeClass('heart');
            $this.tooltip('hide')
                  .attr('data-original-title', 'Retirer de mes favoris')
                  .tooltip('show');
        }
    }).error(function (data) {
        //data
    });
}

function removeWishlist($this, id)
{
    var url = '/supprimer-wishlist/'+ id;
    $.ajax({
        url: url,
        method: 'GET',
        data: {},
    }).success(function (data) {
        if (data.success) {
            var item = $this.parents().find('.product-item');
            if (item.hasClass('wishlist-item')) {
                $this.parent().parent().parent().remove();
            }
            else {
                $this.addClass('heart');
                $this.removeClass('ico_ico_solidheart');
                $this.tooltip('hide')
                  .attr('data-original-title', 'Ajouter à mes favoris')
                  .tooltip('show');
            }
        }
    }).error(function (data) {
        //data
    });
}