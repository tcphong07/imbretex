$(document).ready(function ($) {
    $('body').on('click', 'a.download-document', function (e) {
        e.preventDefault();
        var id = document.getElementById('select-document-tech').value;
        downloadDoc(id);
    });

    $('body').on('click', 'a.certificate-doc', function (e) {

        e.preventDefault();
        var id = document.getElementById('select-certificate-doc').value;
        downloadDoc(id);
    });

    $('body').on('click', 'a.catalog-doc', function (e) {
        e.preventDefault();
        var id = document.getElementById('select-catalog-doc').value;
        console.log('id')
        downloadDoc(id);
    });

    $('body').on('click', 'a.main-visual-doc', function (e) {
        e.preventDefault();
        var id = document.getElementById('select-main-visual-doc').value;
        console.log('id')
        downloadDoc(id);
    });


    $('.container-select').on('click', '.color-visual-doc', function (e) {
        e.preventDefault();
        var id = document.getElementById('select-color-visual').value;
        downloadDocZip(id);
    });

    function downloadDocZip(id) {
        var url = "/produits/telecharger/" + id + "/visuels_couleur";
        window.open(url, '_blank');

    }

    function downloadDoc(id) {
        var url = "/produits/telecharger-doc/" + id;
        $.ajax({
            url: url,
            method: 'get',
            data: {
                'id': id,

            },
        }).success(function (data) {
            window.open(data, '_blank');
            //   window.location.href = data;
        }).error(function (data) {

        });
    }

    var directory_field
    var selectDirectory = $("#select-directory").select2({
        tags: true,
        width: 'resolve',

    });
    $("#select-directory").on("click", function () {
        var newStateVal = $("#new-select-directory").val();
        // Set the value, creating a new option if necessary
        if ($("#select-directory").find("option[value='" + newStateVal + "']").length) {
            $("#select-directory").val(newStateVal).trigger("change");
        } else {
            // Create the DOM option that is pre-selected by default
            var newState = new Option(newStateVal, newStateVal, true, true);
            // Append it to the select
            $("#select-directory").append(newState).trigger('change');
        }
    });

    /*    $('.heart-icon').on("click", function(){

            var heart = document.getElementById('heart');

            if(heart.className == "ico solid_heart"){
                heart.className = "ico heart";
            }
            else{
                heart.className = "ico solid_heart";
            }

        });*/


    $('.clr-link').click(function () {
        var page = $(this).attr('href'); // Page cible
        var speed = 750; // Durée de l'animation (en ms)
        $('html, body').animate({scrollTop: $(page).offset().top}, speed); // Go
        return false;

    });

    directory_field = selectDirectory.val();

    selectDirectory.on("change", function (e) {
        directory_field = $(e.currentTarget).val();
    });

    $('.color-list').on('click', 'a.clr', function () {
        var site_ecommerce = 1;
        var auth = $(this).data('auth');
        site_ecommerce = $(this).data('site-ecommerce');
        $('.color-list .clr-item').removeClass('active');
        $(this).parent().addClass('active');
        $('.colors-slider-wrap').addClass('has-slider').removeClass('colors-lst');
        $('.prd-colors-search').show();
        $('.color-submit').show();
        var id = $(this).data('id');
        getVisual(id, '', 0);

        if (auth == 1) { // on affiche les articles que si l'utilisateur est auth
            getInfos(id, '', site_ecommerce);

        }
        // getGram(id,'');

        $('#main-visual').removeClass('d-none');
    });

    $('body').on('click', '.hidden-not-connected', function () {
        var e = document.getElementById('foo');
        e.style.display = 'none';
    });

    $('body').on('click', '#all-color', function () {
        $('#stock-tab').trigger('click');
    });

    $('body').on('click', '#all-certificates', function () {
        $('#descriptif-tab').trigger('click');
    });

    $('body').on('click', '.tag-class', function () {
        displayColorTag($(this));
    });

    $('#main-visual').click(function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        $('.color-list .clr-item').removeClass('active');
        getVisual(id, '', 1);

        $('#main-visual').addClass('d-none');
    });

    $('#addCart').click(function () {
        addCart(directory_field);
    });

    $('.colors-exists-list').on('click', '.clr-item', function () {
        var id = $(this).data('id');
        var json = $(this).data('quantity');
        var site_ecommerce = 1;
        getInfos(id, json, site_ecommerce);
        $('.color-submit').show();

    });
    $('.btn-search').click(function () {
        var id = $(this).data('id');
        searchColorProduct(id);
    });
    $('#search-field').keyup(function () {
        var id = $(this).data('id');
        searchColorProduct(id);
    });

    $('ul#imbProdSliderTab li:first a.nav-link').trigger('click');

    $('.preloader').hide();
    $('.download-doc a').click(function () {
        //    $('.preloader').show();
    })

});

function searchColorProduct(id) {
    var url = "/produits/search-color-product/" + id;
    var color = document.getElementById('search-field').value;
    var parent_id = id;
    $.ajax({
        url: url,
        method: 'get',
        data: {
            'color': color,
            'parent_id': parent_id,
        },
    }).success(function (data) {
        $('.color-search-product').empty().append(data);

        $('.tbl-colors').empty();
        $('.color-list a').click(function () {
            $('.color-list .clr-item').removeClass('active');
            $(this).parent().addClass('active');
            $('.colors-slider-wrap').addClass('has-slider').removeClass('colors-lst');
            $('.prd-colors-search').show();
            $('.color-submit').show();

            var id = $(this).data('id');
            var site_ecommerce = 1;
            getInfos(id, '', site_ecommerce);
            getVisual(id, '', 0);
            // getGram(id,'');

        });

    }).error(function (data) {

    });

}

function getInfos(id, json, site_ecommerce) {
    var url = '/produits/' + id + '/infos';

    $.ajax({
        url: url,
        method: 'GET',
        data: {'json': json},
    }).success(function (data) {
        if (data) {

            $('#color_infos').html(data);
            if (site_ecommerce == 1) {
                customInputNumber();
                showCardLines();
            }

            $('[data-toggle="tooltip"]').tooltip({
                html: true,
                animation: true,
                delay: {show: 10, hide: 10}
            });
        }
    }).error(function (data) {
        //data
    });
}


function getVisual(id, json, main) {
    var url = '/produits/' + id + '/visuals';
    $.ajax({
        url: url,
        method: 'get',
        data: {'json': json, 'main': main},
    }).success(function (data) {
        $('.block-feature-images').empty().append(data);

        if (!$('.feature-prd-images').length
            || !$('.feature-nav-images').length) {
            return;
        }

        $('.feature-prd-images').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true,
            infinite: false,
            asNavFor: '.feature-nav-images',
            responsive: [
                {
                    breakpoint: 901,
                    settings: {
                        dots: true
                    }
                },
            ]
        });
        $('.feature-nav-images').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            variableWidth: true,
            asNavFor: '.feature-prd-images',
            dots: false,
            centerMode: false,
            focusOnSelect: true,
            infinite: false,
            arrows: false
        });
        if (!$(".prd-image-item").length) {
            return;
        }

        $('.feature-prd-images').lightGallery({
            selector: '.prd-image-item',
            zoom: false,
            share: false,
            autoplay: false,
            download: false,
            autoplayControls: false,
            mode: "slide",
            speed: 400
        });
    }).error(function () {
        //toastr.error('Une erreur est survenue lors du déplacement');
    });
}

function getGram(id, json) {
    var url = '/produits/' + id + '/grams';
    $.ajax({
        url: url,
        method: 'get',
        data: {'json': json},
    }).success(function (data) {
        $('.gram-color').empty().append(data);
    }).error(function () {
        //    toastr.error('Une erreur est survenue lors du déplacement');
    });
}

function addCart(directory_field) {
    var json = [];
    var product_id = [];
    var quantity = [];
    var input_quantity;
    var product_parent = document.getElementById("product-parent");
    var parent_id = product_parent.value;

    //  console.log(directory_field);
    $('.quantity').each(function (index) {

        if ($(this).val() > 0) {
            item = {'product_id': $(this).parent().data('id'), 'quantity': $(this).val()};
            json.push(item);
            product_id.push($(this).parent().data('id'));
            quantity.push($(this).val());
        }
    });
    // console.log(product_id,quantity);

    var token = $('meta[name="csrf-token"]').attr('content');

    var url = '/panier/ajout';
    if (quantity.length > 0) {
        $.ajax({
            url: url,
            method: 'POST',
            data: {
                '_token': token,
                'product_id': product_id,
                'quantity': quantity,
                'directory_field': directory_field,
                'parent_id': parent_id,
                'return_templates': ['cart-header', 'popup-cart-detail']
            },
        }).success(function (data) {
            toastr.options = {positionClass: "toast-top-right"};
            var message = document.getElementById('add-product-cart').value;
            toastr.success(message);
            showCardLines(parent_id);
            HegydEcommerce.updateCartViews(data);
            $('#color_infos').html('');
            $('.color-submit').hide();


        }).error(function (data) {

        });
    }
}

function showCardLines(product_id) {
    var url = '/panier/cartline';
    $.ajax({
        url: url,
        method: 'GET',
        data: {'product_id': product_id,},
    }).success(function (data) {
        if (data) {
            $('.colors-exists-list').html(data).show();
        }
    }).error(function (data) {

    });
}

function customInputNumber() {
    if (!$('.custom-number').length) {
        return;
    }

    $('<div class="button-number up"><i class="ico_ico_arrow_up"></i></div><div class="button-number down"><i class="ico_ico_arrow_down"></i></div>').insertAfter('.custom-number .ipt_num');

    $('.custom-number').each(function (i) {
        var $input = $(this).find('.ipt_num')
        btn_down = $(this).find('.button-number.down'),
            btn_up = $(this).find('.button-number.up'),
            data_min = $input.attr('min'),
            data_max = $input.attr('max'),
            data_step = $input.attr('step');

        btn_up.on('click', function (e) {
            e.preventDefault();
            var oldValue = parseInt($input.val()) ? parseInt($input.val()) : 0;
            var newVal;
            if (oldValue >= data_max) {
                newVal = oldValue;
            } else {
                newVal = oldValue + parseInt(data_step);
            }

            $input.val(newVal);
            $input.trigger("change");
        })

        btn_down.on('click', function (e) {
            e.preventDefault();
            var oldValue = parseInt($input.val()) ? parseInt($input.val()) : 0;
            var newVal;
            if (oldValue <= data_min) {
                newVal = 0;
            } else {
                newVal = oldValue - parseInt(data_step);
            }

            $input.val(newVal);
            $input.trigger("change");
        })
    });

}

function displayColorTag(element) {
    var e = document.getElementById('foo');
    if (e.style.display == 'block')
        e.style.display = 'none';
    else
        e.style.display = 'block';

    var id = element.data('product-id');
    var tag_id = element.data('tag-id');
    var url = "/produits/get-article-by-tag/" + id;
    $.ajax({
        url: url,
        method: 'get',
        data: {
            // 'id' : element.data('product-id'),
            'tag_id': element.data('tag-id'),
        },
    }).success(function (data) {
        $('.tag-infos').empty().append(data);
        $('.clr-item a').click(function () {
            $('.clr-item').removeClass('active');
            element.parent().addClass('active');
            $('.colors-slider-wrap').addClass('has-slider').removeClass('colors-lst');
            $('.prd-colors-search').show();
            $('.color-submit').show();

            var id = $(this).data('id');
            var site_ecommerce = 1;
            getInfos(id, '', site_ecommerce);
            getVisual(id, '', 0);
            $('html, body').animate({scrollTop: $('#stock').offset().top}, 'slow');
            // getGram(id,'');

        });
        $('.clr-name').click(function () {
            // $('.clr-item').removeClass('active');
            element.parent().addClass('active');
            $('.colors-slider-wrap').addClass('has-slider').removeClass('colors-lst');
            $('.prd-colors-search').show();
            $('.color-submit').show();

            var id = $(this).data('id');
            var site_ecommerce = 1;
            getInfos(id, '', site_ecommerce);
            getVisual(id, '', 0);
            $('html, body').animate({scrollTop: $('#stock').offset().top}, 'slow');
            // getGram(id,'');
        });
    });
}
